"""contact_control URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from apps.contacts.views import index, filter_countries, contacts_country, dashboard
from apps.contacts.class_view import ContactList, ContactUpdate, ContactCreate, ContactDelete, CountryList, CountryUpdate, CountryCreate, CountryDelete

urlpatterns = [
    path('admin/', admin.site.urls),
    path("",                            index,                      name="index"),
    path("contacts",                    ContactList.as_view(),      name="contacts"),
    path("update_contact/<int:pk>/",    ContactUpdate.as_view(),    name="update_contact"),
    path("create_contact",              ContactCreate.as_view(),    name="create_contact"),
    path("delete_contact/<int:pk>/",    ContactDelete.as_view(),    name="delete_contact"),

    path("countries",                   CountryList.as_view(),      name="countries"),
    path("update_country/<int:pk>/",    CountryUpdate.as_view(),    name="update_country"),
    path("create_country",              CountryCreate.as_view(),    name="create_country"),
    path("delete_country/<int:pk>/",    CountryDelete.as_view(),    name="delete_country"),

    path("filter_countries",            filter_countries,           name="filter_countries"),
    path("contacts_country/<int:country_id>/",    contacts_country, name="contacts_country"),

    path("dashboard",                    dashboard,                 name="dashboard"),
]
