# Control de Contactos

Proyecto que permite la administración de contactos y países. Filtrar los contactos por país asignado. También tiene la funcionalidad de realizar búsquedas de contactos y países, visualizar estadísticas relevantes del sistema.

# Repositorio

[https://gitlab.com/luissucuco/contact-control](https://gitlab.com/luissucuco/contact-control)

# Publicación

Se puede realizar las pruebas del sistema en [contacts.luissucuc.com](https://contacts.luissucuc.com)

## Tecnologías utilizadas en publicación

- Docker
- nginx
- Certbot
- Digital Ocean

# Tecnologías implementadas

- Python 3
- Django
- Chart.js
- DataTables
- Docker
- Docker Compose
- Bootstrap
- SQLite

# Iniciar proyecto

La ejecución del proyecto se puede utilizar de dos distintas formas: Iniciar un contendor de Docker o iniciar el servidor de forma tradicional (runserver). Para ambas opciones el sistema se iniciará en el puerto [localhost:8000](http://localhost:8000/) y se creará un superuser con los datos:

- Usuario: admin
- E-mail: admin@admin.com
- Password: admin@admin.com

## Ejecución en ambiente Docker

### Variables de Entorno

Las variables de entorno que proporcional las distintas configuraciones para el contenedor de Docker, estas ubicadas en el archivo `.env`:

- **SECRET_KEY**: Clave secreta de Django.
- **DJANGO_DEBUG**: Configuración de debug de Django.
- **ALLOWED_HOSTS**: Host permitidos para acceder al sistema.
- **DB_ENGINE**: Motor de base de datos.
- **DB_NAME**: Nombre de base de datos.
- **ADMIN_USERNAME**: Nombre de usuario para superadmin.
- **ADMIN_EMAIL**: Correo Electrónico de usuario para superadmin.
- **ADMIN_PASS**: Contraseña de usuario para superadmin.

### Dependencias

- Docker
- Docker Compose

Ubicarse en la raíz del proyecto y ejecutar

```bash
docker-compose up -d
```

Se creará una imágen de nombre `control_contacts:latests` y el contendor `control_contacts` ejecutando las configuraciones del archivo `.env`

## Ejecución en ambiente por defecto

### Dependencias

- Django>=3.1.6
- django-crispy-forms>=1.9.0
- django-environ>=0.4.5

### Iniciar proyecto

```bash
python3 manage.py makemigration
```

```bash
python3 manage.py migrate
```

Inserta un superadmin con los datos anteriormente descritos.

```bash
python3 manage.py initadmin
```

```bash
python3 manage.py runserver
```

##### Elaborado por [Luis Sucuc](https://luissucuc.com) - Full Stack Developer
