from django.db import models
from datetime import datetime


# Create your models here.
class Country(models.Model):
    id        = models.AutoField(primary_key=True)
    name      = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


# Create your models here.
class Contact(models.Model):
    id        = models.AutoField(primary_key=True)
    name      = models.CharField(max_length=100)
    last_name = models.CharField(max_length=120)
    country   = models.ForeignKey(Country, on_delete=models.DO_NOTHING )
    email     = models.EmailField(max_length=254, unique=True)
    date      = models.DateField(default=datetime.now, editable=False)
    def __str__(self):
        return self.name
