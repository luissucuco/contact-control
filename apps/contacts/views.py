from django.shortcuts import render
from .models import Country, Contact
from django.contrib.auth.models import User

# Create your views here.

def index(request):
    context = {}
    return render(request, 'base.html', context)

def filter_countries(request):
    context = { 'countries' :  Country.objects.all() }
    return render(request, 'filter_countries.html', context)

def contacts_country(request, country_id):
    context = { 'contacts' :  Contact.objects.filter(country = country_id) }
    return render(request, 'contacts_country.html', context)

def dashboard(request):
    labels_country = []
    data_country   = []
    
    countries = Country.objects.all()
    for country in countries:
        labels_country.append(country.name)
        data_country.append(Contact.objects.filter(country = country.id).count())


    labels_contacts = []
    data_contacts    = []
    dates = Contact.objects.all().order_by('date').values('date').distinct()
    for date in dates:
        labels_contacts.append(str(date["date"]))
        data_contacts.append(Contact.objects.filter(date = date["date"]).count())


    context = {
                'labels_country' : labels_country,
                'data_country'   : data_country,
                'labels_contacts': labels_contacts,
                'data_contacts'  : data_contacts,
                'total_contacts' : Contact.objects.all().count(),
                'total_countries': countries.count(),
                'users_system'   : User.objects.all().count(),
                }

    return render(request, 'dashboard.html', context)
