
import os
import environ


from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

User = get_user_model()
env = environ.Env()

class Command(BaseCommand):

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            username = env.str('ADMIN_USERNAME', default="admin")
            email_ = env.str('ADMIN_EMAIL', default="admin@admin.com")
            pass_ = env.str('ADMIN_PASS',  default="admin@admin.com")

            print('Creating account for %s (%s)' % (username, email_))
            admin = User.objects.create_superuser(email=email_, username=username, password=pass_)
            admin.save()
        else:
            print('Admin already exist')