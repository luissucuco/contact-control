from django.shortcuts import render, redirect
from .models import Contact, Country
from .forms import ContactForm, CountryForm

from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from django.urls import reverse_lazy


class ContactList(ListView):
    model = Contact
    template_name = 'contacts.html'

    def get_queryset(self):
        return self.model.objects.all()
    

class ContactCreate(CreateView):
    model = Contact
    form_class = ContactForm
    template_name = 'update_contact.html'
    success_url = reverse_lazy('contacts')

class ContactUpdate(UpdateView):
    model = Contact
    form_class = ContactForm
    template_name = 'update_contact.html'
    success_url = reverse_lazy('contacts')


class ContactDelete(DeleteView):
    model = Contact
    template_name = 'delete_contact.html'
    success_url = reverse_lazy('contacts')


class CountryList(ListView):
    model = Country
    template_name = 'countries.html'

    def get_queryset(self):
        return self.model.objects.all()
    

class CountryCreate(CreateView):
    model = Country
    form_class = CountryForm
    template_name = 'update_country.html'
    success_url = reverse_lazy('countries')

class CountryUpdate(UpdateView):
    model = Country
    form_class = CountryForm
    template_name = 'update_country.html'
    success_url = reverse_lazy('countries')


class CountryDelete(DeleteView):
    model = Country
    template_name = 'delete_country.html'
    success_url = reverse_lazy('countries')
