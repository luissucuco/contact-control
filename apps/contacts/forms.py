from django import forms
from .models import Contact, Country

class ContactForm(forms.ModelForm):
    country = forms.ModelChoiceField(queryset=Country.objects.all().order_by('name'))
    class Meta:
        model = Contact
        fields = '__all__'
        labels = {
            'name' : 'Nombre',
            'last_name' : 'Apellido',
            'country' : 'País',
            'email' : 'Correo electrónico',
        }
    
class CountryForm(forms.ModelForm):
    class Meta:
        model = Country
        fields = '__all__'
        labels = {
            'name' : 'Nombre',
        }